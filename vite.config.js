import { VitePWA } from "vite-plugin-pwa";
import ViteComponents from "vite-plugin-components";
const { createVuePlugin } = require("vite-plugin-vue2");

module.exports = {
  server: {
    proxy: {
      "/api": {
        target: "https://newsdata.io",
        changeOrigin: true,
        secure: false,
        //rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
  plugins: [
    createVuePlugin(),
    ViteComponents(),
    VitePWA({
      includeAssets: [
        "favicon.svg",
        "favicon.ico",
        "robots.txt",
        "apple-touch-icon.png",
      ],
      manifest: {
        name: "R0N1n-dev Vite PWA",
        short_name: "VVPWA",
        description: "Test for Vite PWA",
        theme_color: "#ffffff",
        icons: [
          {
            src: "pwa-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
            purpose: "any maskable",
          },
        ],
      },
    }),
  ],
};
