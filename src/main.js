import Vue from "vue";
import { registerSW } from "virtual:pwa-register";
import Inkline from "@inkline/inkline";
import "@inkline/inkline/dist/inkline.css";
import "vue-awesome/icons/home";
import "vue-awesome/icons/comment-dollar";
import "vue-awesome/icons/cogs";
import "vue-awesome/icons/sync-alt";
import "vue-awesome/icons/window-close";
import Icon from "vue-awesome/components/Icon";
import App from "./App.vue";

const updateSW = registerSW({
    onNeedRefresh() {},
    onOfflineReady() {},
});

Vue.component("v-icon", Icon);

Vue.use(Inkline);
new Vue({
    render: (h) => h(App),
}).$mount("#app");